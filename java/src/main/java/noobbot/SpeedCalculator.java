package noobbot;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import noobbot.models.CarPosition;
import noobbot.models.Race;
import noobbot.models.Track;

import java.util.List;
import java.util.Map;

/**
 * User: vmaubert
 * Date: 19/04/14
 * Time: 12:41
 */
public class SpeedCalculator {

    private CarPosition previousCarPosition = null;
    private Race race;
    float speedCommand = 1f;
    Map<Integer, List<Float>> maxSpeedByPiece = Maps.newHashMap();
    List<Track.Piece> interestingBendList = Lists.newArrayList();
    private Track.Piece nextBend = null;
    private boolean noMoreBend = false;
    private float distanceToNextBend;

    private float breakCoeff = -0.0033f;
    private float bendCoeff = 0.0045f;
    private boolean bendCoeffCalculated = false;

    private float previousError = 0;
    private float integral = 0;

    public SpeedCalculator(Race race) {
        this.race = race;
        initData(race);
    }

    private void initData(Race race) {
        for (int i = 0; i < race.nbPieces; i++) {
            Track.Piece piece = race.track.pieces.get(i);
            List<Float> maxSpeedList = Lists.newArrayList();
            for (Track.Lane lane : race.track.lanes) {
                maxSpeedList.add(getMaxSpeed(piece, lane.distanceFromCenter));
            }
            maxSpeedByPiece.put(i, maxSpeedList);
        }

        List<Track.Piece> bendList = Lists.newArrayList();
        Track.Piece lastPiece = race.track.pieces.get(race.nbPieces - 1);
        for (Track.Piece piece : race.track.pieces) {
            if (piece.isBend()) {
                if (bendList.isEmpty()) {
                    bendList.add(piece);
                } else if (!lastPiece.isBend() || lastPiece.radius > piece.radius) {
                    bendList.add(piece);
                }
            }
            lastPiece = piece;
        }

        for (Track.Piece currentBend : bendList) {

            if (interestingBendList.isEmpty()) {
                interestingBendList.add(currentBend);
            } else {
                Track.Piece lastBend = interestingBendList.get(interestingBendList.size() - 1);
                boolean needMoreDistance = needMoreDist(currentBend, lastBend);

                if (needMoreDistance) {
                    //Si ce n'est pas le cas on ignore le virage précédent,
                    //car il faut freiner avant le premier virage pour ne pas sortir dans le 2ème
                    interestingBendList.remove(lastBend);
                }
                interestingBendList.add(currentBend);
            }
        }

        //On vérifie qu'il ne faut pas freiner avec le(s) dernier(s) virage(s) pour ne pas arriver trop vite dans le premier virage du circuit
        boolean ok = false;
        while (!ok && interestingBendList.size() > 1) {
            Track.Piece lastBend = interestingBendList.get(interestingBendList.size() - 1);
            Track.Piece currentBend = interestingBendList.get(0);
            boolean needMoreDistance = needMoreDist(currentBend, lastBend);
            if (needMoreDistance) {
                //Si ce n'est pas le cas on ignore le virage précédent,
                //car il faut freiner avant le premier virage pour ne pas sortir dans le 2ème
                interestingBendList.remove(lastBend);
            } else {
                ok = true;
            }
        }
    }

    private boolean needMoreDist(Track.Piece currentBend, Track.Piece lastBend) {
        //On calcule la distance entre la début du premier virage et le deuxième.
        float distance = getDistanceBetween2Pieces(lastBend.index, 0, currentBend.index);

        float lastBendMaxSpeed = maxSpeedByPiece.get(lastBend.index).get(0);
        float currentBendMinSpeed = maxSpeedByPiece.get(currentBend.index).get(0);
        for (int i = 1; i < race.track.lanes.size(); i++) {
            lastBendMaxSpeed = Math.max(maxSpeedByPiece.get(lastBend.index).get(i), lastBendMaxSpeed);
            currentBendMinSpeed = Math.min(maxSpeedByPiece.get(currentBend.index).get(i), currentBendMinSpeed);
        }

        //On calcule la distance pour freiner si on sort du dernier virage en vitesse max
        float breakDistance = getBreakDistanceBetween2Speed(lastBendMaxSpeed, currentBendMinSpeed);

        //Cette distance est-elle suffisante pour freiner ?
        return breakDistance > distance;
    }

    public float getSpeedCommand(CarPosition myCarPosition, int currentPieceIndex, float currentSpeed) {

        Track.Lane currentLane = race.track.lanes.get(myCarPosition.piecePosition.lane.endLaneIndex);
        Track.Piece currentPiece = race.track.pieces.get(currentPieceIndex);
        speedCommand = getMaxSpeed(currentPiece, currentLane.distanceFromCenter);

        nextBend = getNextBend(currentPieceIndex, myCarPosition.piecePosition.lap);
        System.out.print("\n" + currentSpeed);

        if (noMoreBend) {
            if (!currentPiece.isBend()) {
                speedCommand = 1f;
            }
        } else {
            //On calcule la distance a effectuer pour atteindre le prochain virage.
            distanceToNextBend = getDistanceBetween2Pieces(currentPieceIndex, myCarPosition.piecePosition.inPieceDistance, nextBend.index);

            //On calcule la distance de freinage pour arriver dans ce virage à la vitesse max
            float breakDistance = getBreakDistanceBetween2Speed(currentSpeed, maxSpeedByPiece.get(nextBend.index).get(currentLane.index));

            //Si cette distance n'est pas suffisante, on freine
            if (breakDistance > distanceToNextBend) {
                speedCommand = 0f;
            }
        }


        if (speedCommand > 1) {
            speedCommand = 1;
        }
        if (speedCommand < 0) {
            speedCommand = 0f;
        }

        float realAngle = Math.abs(myCarPosition.angle);

        final float error = speedCommand - currentSpeed;
        if (currentSpeed < 1) {
            integral += error * 10;
        }

        if (speedCommand != 1) {
            float kp = 60;
            float ki = 0.002f;
            float kd = 12.5f;
            float derive = (error - previousError) / 10;
            speedCommand = kp * error + ki * integral + kd * derive;
            previousError = error;

            //Dans le cas où on est à l'équilibre, on peut vérifier si l'angle est bon ou non
            if (!bendCoeffCalculated && realAngle < 52 && derive < 0.0001 && currentPiece.isBend()) {
                bendCoeff += 0.000002;
            }
        }
        if (realAngle > 52) {
            bendCoeffCalculated = true;
            if (realAngle > 54) {
                bendCoeff -= 0.000005;
            }
        }

        if (speedCommand > 1) {
            speedCommand = 1;
        }
        if (speedCommand < 0) {
            speedCommand = 0f;
        }
        return speedCommand;
    }

    /**
     * @param currentPieceIndex
     * @param currentLap
     * @return le prochain virage où l'on doit freiner pour le prendre
     */
    public Track.Piece getNextBend(int currentPieceIndex, int currentLap) {
        if (!noMoreBend) {
            nextBend = interestingBendList.get(0);
            boolean previousBendFound = false;
            for (Track.Piece bend : interestingBendList) {
                if (bend.index > currentPieceIndex) {
                    if (previousBendFound) {
                        nextBend = bend;
                    }
                    break;
                } else {
                    previousBendFound = true;
                }
            }

            //Attention en qualif le raceSession.laps == 0
            //TODO optimiser en qualif, si je n'ai pas le temps de faire un tour supplémentaire,
            // on doit foncer le plus vite possible vers la ligne d'arrivée sans freiner
            if (race.raceSession.laps != 0 && race.raceSession.laps == currentLap + 1 && nextBend.index <= currentPieceIndex) {
                //Si c'est la fin dernier tour, il n'y a pas d'autre virage
                noMoreBend = true;
            }
        }

        return nextBend;
    }

    private Float getMaxSpeed(Track.Piece piece, int distanceFromCenter) {
        Float maxSpeed;
        if (!piece.isBend()) {
            maxSpeed = new Float(1);
        } else {
            //En fonction du sens du virage, il faut soit ajouter sous soustraire distanceFromCenter du virage
            int sens = -1;
            if (piece.angle < 0) {
                sens = 1;
            }
            float rayon = piece.radius + sens * distanceFromCenter;
            maxSpeed = new Float(Math.sqrt(rayon * bendCoeff));

        }

        return maxSpeed;
    }

    public float getCurrentSpeed(CarPosition myCarPosition, float previousSpeed) {
        float currentSpeed;
        if (race == null || previousCarPosition == null) {
            currentSpeed = 0;
        } else {
            int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
            int previousPieceIndex = previousCarPosition.piecePosition.pieceIndex;
            float currentPieceDistance = myCarPosition.piecePosition.inPieceDistance;

            //Les calculs sont différents si on a changé de pièce ou pas
            if (currentPieceIndex == previousPieceIndex) {
                float previousPieceDistance = previousCarPosition.piecePosition.inPieceDistance;
                currentSpeed = (currentPieceDistance - previousPieceDistance) / 10f;
            } else {
                Track.Piece previousPiece = race.track.pieces.get(previousCarPosition.piecePosition.pieceIndex);
                Track.Lane previousLane = race.track.lanes.get(previousCarPosition.piecePosition.lane.startLaneIndex);

                //Il y a un bug, la formule marche pas du tout quand la pièce d'avant est un switch !!! Pour pas tomber dedans, on actualise pas la vitesse
                if (!previousPiece.isSwitch) {
                    float previousPieceDistance = previousPiece.getDistance(previousLane.distanceFromCenter) - previousCarPosition.piecePosition.inPieceDistance;
                    currentSpeed = (currentPieceDistance + previousPieceDistance) / 10f;
                } else {
                    currentSpeed = previousSpeed;
                }
            }
        }
        previousCarPosition = myCarPosition;
        return currentSpeed;
    }

    /**
     * @param indexPiece1
     * @param distanceOnPiece1AlreadyDone
     * @param indexPiece2
     * @return La distance entre le début de la première pièce et le début de la deuxième
     */
    public float getDistanceBetween2Pieces(int indexPiece1, float distanceOnPiece1AlreadyDone, int indexPiece2) {
        if (indexPiece1 == indexPiece2) {
            return 0;
        }
        float distance = 0f;
        List<Track.Piece> path;
        if (indexPiece1 < indexPiece2) {
            path = race.track.pieces.subList(indexPiece1, indexPiece2);
        } else {
            path = Lists.newArrayList();
            for (int i = indexPiece1; i < race.nbPieces; i++) {
                path.add(race.track.pieces.get(i));
            }
            for (int i = 0; i < indexPiece2 - 1; i++) {
                path.add(race.track.pieces.get(i));
            }
        }
        for (Track.Piece piece : path) {
            float distanceAlreadyDone = 0f;
            if (piece.index == indexPiece1) {
                distanceAlreadyDone = distanceOnPiece1AlreadyDone;
            }
            distance += piece.getDistance(0) - distanceAlreadyDone;
        }
        return distance;
    }

    private float getBreakDistanceBetween2Speed(Float speed1, Float speed2) {
        if (speed1 < speed2) {
            return 0;
        }

        return (((speed2 * speed2) - (speed1 * speed1)) / breakCoeff) + speed1 * 10;
    }

    public void reset(Race race) {
        this.race = race;
        previousCarPosition = null;
        nextBend = interestingBendList.get(0);
        noMoreBend = false;
    }
}
