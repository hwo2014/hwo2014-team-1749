package noobbot.messages;

public class CreateRace extends JoinRace {


    public CreateRace(String name, String key, String trackname, String carCount, String password) {
        super(name, key, trackname, carCount, password);
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

}
