package noobbot.messages;

import com.google.gson.Gson;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:51
 */
public abstract class SendMsg {


    private int gameTick;

    protected SendMsg(int gameTick) {
        this.gameTick = gameTick;
    }

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();

    protected final int gameTick() {
        return gameTick;
    }
}
