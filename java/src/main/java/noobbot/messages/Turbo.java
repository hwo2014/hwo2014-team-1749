package noobbot.messages;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:51
 */
public class Turbo extends SendMsg {

    public Turbo(int gameTick) {
        super(gameTick);
    }

    @Override
    protected Object msgData() {
        return "VROOOOOOOOOOOOOOOM";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }


}
