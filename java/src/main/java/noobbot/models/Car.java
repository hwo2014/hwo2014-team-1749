package noobbot.models;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 14:44
 */
public class Car {

    public CarId id;
    public Dimension dimensions;


    public class Dimension {
        public int length;
        public int width;
        public int guideFlagPosition;
    }
}
