package noobbot.models;


public class CarPosition {

    public CarId id;
    public float angle;
    public PiecePosition piecePosition;

    public class PiecePosition{
        public int pieceIndex;
        public float inPieceDistance;
        public int lap;

        public Lane lane;

        public class Lane{
            public int startLaneIndex;
            public int endLaneIndex;
        }
    }
}
