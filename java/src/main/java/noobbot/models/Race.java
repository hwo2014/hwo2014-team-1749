package noobbot.models;

import java.util.List;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 14:12
 */
public class Race {

    public Track track;
    public RaceSession raceSession;
    public List<Car> cars;
    public int nbPieces = 0;

    public Race(Object data) {
    }


    public class RaceSession {
        public int laps;
        public int maxLapTimeMs;
        public boolean quickRace;
    }
}
