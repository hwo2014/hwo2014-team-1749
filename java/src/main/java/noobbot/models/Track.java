package noobbot.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 14:06
 */
public class Track {

    public String id;
    public String name;
    public List<Piece> pieces;
    public List<Lane> lanes;
    public StartingPoint startingPoint;

    public class Lane {
        public final int index;
        public final int distanceFromCenter;

        Lane(int index, int distanceFromCenter) {
            this.index = index;
            this.distanceFromCenter = distanceFromCenter;
        }
    }

    public class Piece{
        @SerializedName("switch")
        public final boolean isSwitch;
        public final float length;
        public final int radius;
        public final float angle;
        public int index;

        Piece(boolean isSwitch, float length, int radius, float angle) {
            this.isSwitch = isSwitch;
            this.length = length;
            this.radius = radius;
            this.angle = angle;
        }

        public boolean isBend(){
            return length == 0;
        }

        public float getDistance(int distanceFromCenter) {
            if (isBend()) {
                //En fonction du sens du virage, il faut soit ajouter sous soustraire distanceFromCenter du virage
                int sens = -1;
                if (angle < 0) {
                    sens = 1;
                }
                return (float) (2d * Math.PI * (radius + sens * distanceFromCenter) * Math.abs(angle) / 360d);
            }
            return length;
        }
    }

    public class StartingPoint {
        public final Position position;
        public final float angle;

        StartingPoint(Position position, float angle) {
            this.position = position;
            this.angle = angle;
        }

        public class Position {
            public final float x;
            public final float y;

            Position(float x, float y) {
                this.x = x;
                this.y = y;
            }
        }
    }
}
